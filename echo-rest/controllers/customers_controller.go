package controllers

import (
	"echo-rest/handlers"
	"net/http"

	"github.com/labstack/echo/v4"
)

// FetchAllCustomers godoc
// @Summary FetchAll Customers
// @Accept json
// @Produce json
// @Success 200
// @Tag Customers
// @Router /customers/list [post]
func FetchAllCustomers(c echo.Context) (err error) {

	result, err := handlers.FetchCustomers()

	return c.JSON(http.StatusOK, result)
}

func FetchCustomer(c echo.Context) (err error) {

	result, err := handlers.FetchCustomer(c)

	return c.JSON(http.StatusOK, result)
}

//FetchAllCustomers ...
func StoreCustomer(c echo.Context) (err error) {

	result, err := handlers.StoreCustomer(c)

	return c.JSON(http.StatusOK, result)
}

//FetchAllCustomers ...
func UpdateCustomer(c echo.Context) (err error) {

	result, err := handlers.UpdateCustomer(c)

	return c.JSON(http.StatusOK, result)
}

//DeleteCustomer ...
func DeleteCustomer(c echo.Context) (err error) {

	result, err := handlers.DeleteCustomer(c)

	return c.JSON(http.StatusOK, result)
}
