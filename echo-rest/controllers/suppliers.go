package controllers

import (
	"echo-rest/handlers"
	"net/http"

	"github.com/labstack/echo/v4"
)


func FetchSuppliers(c echo.Context) (err error) {

	result, err := handlers.FetchSuppliers()

	return c.JSON(http.StatusOK, result)
}