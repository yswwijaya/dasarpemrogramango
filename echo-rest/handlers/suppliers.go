package handlers

import (
	"echo-rest/common"
	"echo-rest/db"
	"net/http"

	ex "github.com/wolvex/go/error"
)


var supp common.Suppliers
var suppObj []common.Suppliers

func FetchSuppliers() (res Response, err error) {

	defer func() {
		if errs != nil {
			res.Status = errs.ErrCode
			res.Message = errs.Remark
		}
	}()

	con := db.CreateCon()

	sqlQuery := `SELECT
					SupplierID,
					IFNULL(CompanyName,''),
					IFNULL(ContactName,'') ContactName,
					IFNULL(ContactTitle,'') ContactTitle,
					IFNULL(Address,'') Address,
					IFNULL(City,'') City,
					IFNULL(Country,'') Country,
					IFNULL(Phone,'') Phone ,
					IFNULL(PostalCode,'') PostalCode
				FROM suppliers`

	rows, err := con.Query(sqlQuery)

	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {

		err = rows.Scan(&supp.SupplierID, &supp.CompanyName, &supp.ContactName, &supp.ContactTitle, &supp.Address, &supp.City,
			&supp.Country, &supp.Phone, &supp.PostalCode)

		if err != nil {
			errs = ex.Errorc(http.StatusNoContent).Rem(err.Error())
			errMessage = err.Error()
			return res, err
		}

		suppObj = append(suppObj, supp)

	}

	res.Status = http.StatusOK
	res.Message = "succses"
	res.Data = suppObj

	return res, nil
	 
}