package services

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type M map[string]interface{}

func DoRequest(url, method string, data interface{}) ([]byte, error) {
	
	var payload *bytes.Buffer = nil

	if data != nil {
		payload = new(bytes.Buffer)
		err := json.NewEncoder(payload).Encode(data)
		if err != nil {
			return nil, err
		}
	}

	request, err := http.NewRequest(method, url, payload)
	if err != nil {
		return nil, err
	}

	client := new(http.Client)

	response, err := client.Do(request)

	if response != nil {
		defer response.Body.Close()
	}
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(response.Body)

	return body, nil
}