package controllers

import (
	"echo-rest-sqlx/handlers"
	"net/http"

	"github.com/labstack/echo/v4"
)

func FetchTravels(c echo.Context) (err error) {

	result, err := handlers.FetchTravels(c)

	return c.JSON(http.StatusOK, result)
}

func FetchUser(c echo.Context) (err error) {

	result, err := handlers.FetchUser(c)

	return c.JSON(http.StatusOK, result)
}