package handlers

import (
	"echo-rest-sqlx/common"
	"echo-rest-sqlx/db"
	sr "echo-rest-sqlx/services"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	ex "github.com/wolvex/go/error"
)

var errs *ex.AppError
var travel common.TravelList
type bodyRequest map[string]interface{}

func FetchTravels(c echo.Context) (res common.Response, err error) {

	defer func() {
		if errs != nil {
			res.Status = errs.ErrCode
			res.Message = errs.Remark
		}
	}()

	req := new(common.Request)

	if err = c.Bind(req); err != nil {
		return res, err
	}

	baseURL := req.Url
	method := "POST"
	data := bodyRequest{"provinsi": req.Province}
	
	responseBody, err := sr.DoRequest(baseURL, method, data)
	
	if err != nil {
		log.Println("ERROR", err.Error())
		errs.ErrCode =  1
		errs.Remark =  err.Error()
		return
	}
	
	if err = json.Unmarshal(responseBody, &travel); err != nil {
		errs.ErrCode =  1
		errs.Remark =  err.Error()
		return
	}

	var totalData int

	tx := db.DB.MustBegin()
	for i, rec := range travel.Data {
		tx.MustExec("INSERT INTO travels (Trip, Description, TravelID, TravelName, CityName) VALUES (?,?,?,?,?)", rec.TripID, rec.Description, rec.TravelID, rec.TravelName, rec.CityName)
		totalData = i
	}
	tx.Commit()
		    
	res.Status = http.StatusOK
	res.Message = "succses"
	res.Data = map[string]int{
		"total data insert :": totalData,
	}
	
	return
}