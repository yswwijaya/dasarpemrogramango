package handlers

import (
	"echo-rest-sqlx/common"
	sr "echo-rest-sqlx/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	logger "github.com/sirupsen/logrus"
)


var userTravel common.UserTravel

func FetchUser(c echo.Context) (res common.Response, err error) {

	fmt.Printf("test user")

	defer func() {
		if errs != nil {
			res.Status = errs.ErrCode
			res.Message = errs.Remark
		}
	}()

	req := new(common.RequestUser)

	if err = c.Bind(req); err != nil {
		return res, err
	}
   
	baseURL := fmt.Sprintf("%s%d", common.Config.UrlUser, req.UserID)  
	method := "GET"
	data := ""
	
	logger.WithFields(logger.Fields{
		"Request Body": data,
		"Method" : method,
		"url" : baseURL,
	}).Info("Response Body")
	
	responseBody, err := sr.DoRequest(baseURL, method, data)
	
	if err != nil {
		log.Println("ERROR", err.Error())
		errs.ErrCode =  1
		errs.Remark =  err.Error()
		return
	}
	
	if err = json.Unmarshal(responseBody, &userTravel); err != nil {
		errs.ErrCode =  1
		errs.Remark =  err.Error()
		return
	}

	//fmt.Printf(string(responseBody))
	
	logger.WithFields(logger.Fields{
		"Response Body": string(responseBody),
	}).Info("Response from reqres.in")
	
	// var totalData int

	// tx := db.DB.MustBegin()
	// for i, rec := range travel.Data {
	// 	tx.MustExec("INSERT INTO travels (Trip, Description, TravelID, TravelName, CityName) VALUES (?,?,?,?,?)", rec.TripID, rec.Description, rec.TravelID, rec.TravelName, rec.CityName)
	// 	totalData = i
	// }
	// tx.Commit()

	response := &common.ResponseUser{
		UserID: int(userTravel.Data.ID),
		UserName: userTravel.Data.FirstName  + " " + userTravel.Data.LastName,
		Email: userTravel.Data.Email, 
	}

	logger.WithFields(logger.Fields{
		"Last Response": response,
	}).Info("Last Response")

	res.Status = http.StatusOK
	res.Message = "succses"
	res.Data = response

	return res, nil
}
